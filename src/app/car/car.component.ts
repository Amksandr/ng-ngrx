import { Component, OnInit, Input } from '@angular/core';
import { Car } from '../car.model';
import { AppState } from '../redux/app.state';
import { Store } from '@ngrx/store';
import { DeleteCar } from '../redux/cars.actions';
import { CarsService } from '../service/cars.service';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  @Input() car: Car;

  constructor(private store: Store<AppState>, private service: CarsService) { }

  ngOnInit() {
  }

  onBuy() {
    // this.store.dispatch(new DeleteCar(this.car));
    this.car.isSold = true;
    this.service.updateCar(this.car);
  }

  onDelete() {
    console.log('onDelete');
    // this.store.dispatch(new DeleteCar(this.car));
    this.service.deleteCar(this.car);
  }



}
