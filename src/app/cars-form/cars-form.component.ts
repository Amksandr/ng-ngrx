import { Component, OnInit } from '@angular/core';
// import { Store } from '@ngrx/store';
// import { AppState } from '../redux/app.state';

import { Car } from '../car.model';
import * as moment from 'moment';
import { AddCar } from '../redux/cars.actions';
import { CarsService } from '../service/cars.service';

@Component({
  selector: 'app-cars-form',
  templateUrl: './cars-form.component.html',
  styleUrls: ['./cars-form.component.css']
})
export class CarsFormComponent implements OnInit {

  private id = 2;

  carName = '';
  carModel = '';

  constructor(
    // private store: Store<AppState>,
    private service: CarsService) { }

  ngOnInit() {
  }

  onAdd() {
    if (this.carModel === '' || this.carName === '') { return; }
    const date = moment().format('DD.MM.YY');
    const car = new Car(
      this.carName,
      date,
      this.carModel
    );

    // this.store.dispatch(new AddCar(car));
    this.service.addCar(car);

    this.carModel = '';
    this.carName = '';
  }

  onLoad() {
    this.service.loadCars();
  }

}
