// import { Action } from '@ngrx/store';
import { Car } from '../car.model';
import { CAR_ACTION, CarsAction } from './cars.actions';

const initialState = {
    cars: [
        // new Car('Ford', '22.10.17', 'Focus', false, 1),
        // new Car('Audi', '07.02.13', 'A4', false, 2),
    ]
};

export function carsReducer(state = initialState, action: CarsAction) {
    switch (action.type) {
        case CAR_ACTION.ADD_CAR:
            return {
                ...state,
                cars: [...state.cars, action.payload]
            };
        case CAR_ACTION.DELETE_CAR:
            return {
                ...state,
                cars: [...state.cars.filter(c => c.id !== action.payload.id)]
            };
        case CAR_ACTION.UPDATE_CAR:
            const idx = state.cars.findIndex(c => c.id === action.payload.id);
            state.cars[idx].isSold = true;
            // console.log('Reducer idx, state.car: ', idx, state.cars);
            return {
                ...state,
                cars: [...state.cars]
            };
        case CAR_ACTION.LOAD_CARS:
            return {
                ...state,
                cars: [...action.payload]
            };
        default:
            return state;
    }
}
