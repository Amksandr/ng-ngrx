import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';

import { AppState } from '../redux/app.state';
import { LoadCars, AddCar, DeleteCar, UpdateCar } from '../redux/cars.actions';
import { Car, Cars } from '../car.model';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

// import * as map from 'rxjs/operator/map';
// import 'rxjs/add/observable/throw';
// import 'rxjs/add/operator/map';
// import * as map from 'rxjs/add/operator/map';

@Injectable()
export class CarsService {

    static BASE_URL = 'http://localhost:3000/';

    constructor(private http: HttpClient, private store: Store<AppState>) {}

    preloadCars(): Observable<Car[]> {
        return this.http.get(CarsService.BASE_URL + 'cars')
            .pipe(
                map((cars: Car[]) => cars)
            );
    }

    loadCars(): void {
        this.preloadCars()
        // this.http.get(CarsService.BASE_URL + 'cars')
            .subscribe((cars: Car[]) => {
                console.log('PreloadCars[]');
                console.dir(cars);
                this.store.dispatch(new LoadCars(cars));
            });
    }

    addCar(car: Car) {
        this.http.post(CarsService.BASE_URL + 'cars', car)
            .subscribe((carOne: Car) => {
                this.store.dispatch(new AddCar(carOne));
            });
    }

    deleteCar(car: Car) {
        this.http.delete(CarsService.BASE_URL + 'cars/' + car.id)
            .subscribe((carOne: Car) => {
                this.store.dispatch(new DeleteCar(car));
            });
    }

    updateCar(car: Car) {
        console.log('Service upd: ', car);
        this.http.put(CarsService.BASE_URL + 'cars/' + car.id, car)
            .subscribe((response: Car) => {
                // console.log('service update [response] : ', response);
                // console.log('service update car: ', car);
                this.store.dispatch(new UpdateCar(car));
                // this.store.dispatch(new UpdateCar(response));
            });
    }
}
